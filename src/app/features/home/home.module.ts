import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HomeComponent } from './home.component';
import {TableModule} from '../table/table.module';
import {SharedModule} from '../../shared/shared.module';



@NgModule({
  declarations: [HomeComponent],
  imports: [
    CommonModule,
    TableModule,
    SharedModule
  ],
  exports: [HomeComponent]
})
export class HomeModule { }
