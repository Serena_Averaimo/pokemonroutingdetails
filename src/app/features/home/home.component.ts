import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent {

  img1: string = 'https://picsum.photos/200/300';
  img2: string = 'https://picsum.photos/200/301';
  img3: string = 'https://picsum.photos/200/302';


  constructor() { }


}
