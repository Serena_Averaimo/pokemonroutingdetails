import { Component, OnInit } from '@angular/core';
import {Pokemon} from '../../model/pokemon.model';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent{
  loading = true;
  pokemon: Pokemon[];

  constructor() {
    setTimeout(() => {
      this.loading = false;
    }, 3000);

    this.pokemon = [
      {id: 1, name: 'Bulbasaur' , type: 'Grass' },
      {id: 2, name : 'IvySaur', type : 'Grass'},
      {id: 3 , name: 'Venusaur', type: 'Grass'},
      {id: 4, name: 'Charmender' , type: 'Fire'},
      {id: 5 , name: 'Charmeleon' , type: 'Fire'},
      {id: 6 , name: 'Charizard', type: 'Fire'},
      {id: 7, name: 'Squirtle', type : 'Water'},
      {id: 8 , name : 'Warturtle' , type : 'Water'},
      {id: 9 , name: 'Blastoise' , type: 'Water'}
    ];
  }



}
