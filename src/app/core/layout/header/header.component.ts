import { Component, OnInit } from '@angular/core';
import {Pokemon} from '../../../model/pokemon.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent  {
  pokemon: Pokemon[];
  selectedPokemon: string = '';
  searchDisabled = true;

  constructor() {
    this.pokemon = [
      {id: 1, name: 'Bulbasaur' , type: 'Grass'},
      {id: 2, name : 'IvySaur', type : 'Grass'},
      {id: 3 , name: 'Venusaur', type: 'Grass'},
      {id: 4, name: 'Charmender' , type: 'Fire'},
      {id: 5 , name: 'Charmeleon' , type: 'Fire'},
      {id: 6 , name: 'Charizard', type: 'Fire'},
      {id: 7, name: 'Squirtle', type : 'Water'},
      {id: 8 , name : 'Warturtle' , type : 'Water'},
      {id: 9 , name: 'Blastoise' , type: 'Water'}
    ];
  }

  searchPokemon(): void {
   const foundPokemon = this.pokemon.find((el) => {
     // tslint:disable-next-line:no-unused-expression
    return el.name.toLowerCase() === this.selectedPokemon.toLowerCase();
   });
   if (foundPokemon) {
      console.log(foundPokemon);
}
   else{
     console.log('pokemon not found');
   }
   // console.log((foundPokemon) ? foundPokemon : 'Pokemon not found');
  }

  enableButtonSearch($event: any): void {

   const target = $event.target as HTMLInputElement;
   if (target.value.length > 0) {
    this.searchDisabled = false;
    this.selectedPokemon = target.value;
   }
   else {
     this.searchDisabled = true;
   }
  }


}
